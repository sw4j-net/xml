ARG CI_REGISTRY
FROM debian:bookworm-backports

RUN <<EOF
apt-get update
apt-get -y upgrade
apt-get -y install wget xmlstarlet
EOF
