# Introduction

This project creates a docker image with xml utilities and wget installed.

The image can be used as a base image for other images or to process xml files.

This repository is mirrored to https://gitlab.com/sw4j-net/xml
